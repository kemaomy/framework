<?php 
//配置文件
//控制器文件夹
define('CONTROLLER_DIR', 'controller');
define('MODEL_DIR', 'model');



$config['system']['db']=array(
	'dbHost'=>"localhost",
	'dbUser'=>"root",
	'dbPasswd'=>"",
	'dbName'=>"db",
	'dbCharset'=>"utf-8",
	'dbConn'=>"",
	'dbPrefix'=>"tp"

	);

$config['system']['temp']=array(
		'CompileDir'=>'/templates_c/',
		'ConfigDir'=>'/config/',
		'CacheDir'=>'/cache/',
		'TemplateDir'=>'/templates/'
);

$config['system']['setting'] = array(
		'debug'=>'1',//1为开启测试模式，0为关闭
		'url_model'=>'0'//1为pathinfo模式，0为传统get模式
)
?>