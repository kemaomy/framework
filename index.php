<?php
define('ROOT_PATH', str_replace('\\', '/', dirname(__FILE__)));//定义根目录
define('WALL_CODE',true);//防止跳墙访问
/*加载核心模块*/
require ROOT_PATH.'/config.php';//加载配置文件
require ROOT_PATH.'/core/dispatcher.php';//加载核心分发器

$dispatcher = new Dispatcher();//实例化核心分发器
$status=$dispatcher->run($config);
?>
