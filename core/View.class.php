<?php
/**
* 视图父类
*/
defined('WALL_CODE') or exit('No permission resources.'); 
class View{
	protected $_smarty = NULL;
	protected $config = NULL;
	
	function __construct(){
		$this->_smarty = new Smarty();
		$this->config = $this->config();
		
		//parent::__construct();

		//$this->setTemplateDir(ROOT_PATH.'/templates/');
        $this->_smarty->setCompileDir(ROOT_PATH.$this->config['CompileDir']);
        $this->_smarty->setConfigDir(ROOT_PATH.$this->config['ConfigDir']);
        $this->_smarty->setCacheDir(ROOT_PATH.$this->config['CacheDir']);

        $this->_smarty->caching = Smarty::CACHING_LIFETIME_CURRENT;

        //var_dump($this->_smarty);
	}
	public function _setTemplateDir($dir){
		//echo "setTemplateDir";
		$this->_smarty->setTemplateDir(ROOT_PATH.$this->config['TemplateDir'].$dir.'/');
	}
	//获取smarty实例
	final public function _getSmarty(){
		return $this->_smarty;
	}
	final protected function config() {
		return Dispatcher::$_config['temp'];
	}
}
?>