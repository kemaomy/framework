<?php
/**
* 控制器基类
*/
defined('WALL_CODE') or exit('No permission resources.'); 
class Controller{
	
	protected $model = NULL;
	protected $view = NULL;
	protected $smarty = NULL;

	function __construct(){
		//默认实例化Model类时实例化View类
		$this->view = new View;
		$this->smarty = $this->view->_getSmarty();
		
		//$this->load_model("index");
	}
	//通过model_name载入对应模型
	final public function load_model($model_name){
		$model_name.='Model';
		$this->model = new $model_name;
		return $this->model;
	}
	//通过action名称载入对应的模板文件
	final public function display($tmp_dir ,$tmp_name){
		//$this->view = new View;
		//echo "this is smarty display";
		$this->view->_setTemplateDir($tmp_dir);
		//print_r($this->smarty);
		$this->smarty->display($tmp_name);
	}
	//将值分配到模板
	final public function assign($key ,$value){
		//echo "this is smarty assign";
		//$this->view = new View;
		$this->smarty->assign($key ,$value);
	}
}

?>