<?php
/**
* 模型父类
*/
defined('WALL_CODE') or exit('No permission resources.'); 
class Model extends Sql{
	//数据库连接
	protected $link = NULL;
//	protected $tableName;//模型对应的表名

	function __construct($tableName){
		parent::__construct($tableName);
	}
	
	//载入配置参数
	final protected function config(){
		return Dispatcher::$_config['db'];
	}
	//设置数据库编码
	final protected function setCharset($link,$charset){
		$link->query("set character set '$charset'");
	}

	public function setTableName() {
		parent::set_table_name($this->tableName);
	}

}

?>